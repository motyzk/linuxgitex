import pathlib
def txtsInDir(file):
    folder = pathlib.Path('.')
    filelist = []
    for file_or_dir in folder.iterdir():
        if file_or_dir.is_file():
            if pathlib.Path(file_or_dir).suffix == '.txt':
                filelist.append(str(file_or_dir))
    filelist.sort()
    for item in filelist:
        file.write(item + "\n")

with open("contents.txt", "w") as file:
    txtsInDir(file)
